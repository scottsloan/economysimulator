﻿using EconomySimulator.Enums;
using EconomySimulator.Interfaces;

namespace EconomySimulator.Models
{
    public class Person : IActor
    {
        public const int WATER_USAGE_LIFE = 1;
        public const int FOOD_USAGE_LIFE = 1;

        private Guid _id = Guid.NewGuid();

        public string Identifier
        {
            get { return _id.ToString(); }
        }

        public string Name { get; set; }
        public Dictionary<Resource, decimal> Resources { get; set; }
        public List<TransactionEntry> TransactionEntries { get; set; }

        public Resource Outputs { get; protected set; }

        public Person()
        {
            Resources = new Dictionary<Resource, decimal>
            {
                { Resource.Money, 0 },
                { Resource.Water, 0 },
                { Resource.Food, 0 },
                { Resource.Wood, 0 }
            };

            Name = $"Random Person {DateTime.Now.Ticks}";

            TransactionEntries = new List<TransactionEntry>();
        }

        public Person(string name)
            : this()
        {
            this.Name = name;
        }

        public bool IsAlive()
        {
            return GetSupplyForResource(Resource.Food) >= 0 && GetSupplyForResource(Resource.Water) >= 0;
        }

        public override string ToString()
        {
            return this.Name;
        }

        public virtual bool HasDemandFor(Resource resource)
        {
            return GetDemandForResource(resource) > 0;
        }

        public virtual void Purchases(Resource resource, decimal Qty, decimal Price)
        {
            this.Resources[resource] += Qty;
            this.Resources[Resource.Money] -= Price;

            TransactionEntries.Add(TransactionEntry.CreateEntry(MarketAction.Buy, resource, Qty, Price));
        }

        public virtual bool CanPurchase(Resource resource, decimal Qty, decimal Price)
        {
            return this.GetSupplyForResource(Resource.Money) >= Price;
        }

        public virtual decimal GetSupplyForResource(Resource resource)
        {
            return this.Resources[resource];
        }

        public virtual decimal GetDemandForResource(Resource resource)
        {
            decimal amt = this.GetSupplyForResource(resource);

            if (resource == Resource.Water)
            {
                return WATER_USAGE_LIFE - amt;
            }
            else if (resource == Resource.Food)
            {
                return FOOD_USAGE_LIFE - amt;
            }

            return 0;
        }

        public virtual List<Resource> GetResourceNeeds()
        {
            List<Resource> _needs = new List<Resource>();

            foreach(var r in Enum.GetValues<Resource>())
            {
                if (this.HasDemandFor(r))
                {
                    _needs.Add(r);
                }
            }

            return _needs;
        }

        public virtual void RunIteration()
        {
            this.Resources[Resource.Water] -= WATER_USAGE_LIFE;
            this.Resources[Resource.Food] -= FOOD_USAGE_LIFE;

            this.Work();
        }

        public virtual decimal GetQtyToPurchase(Resource resource, decimal Price)
        {
            decimal need = this.GetDemandForResource(resource);
            decimal money = this.GetSupplyForResource(Resource.Money);

            if(need * Price > money)
            {
                return (int)(money / Price);
            }

            return need;
        }

        public virtual void Work()
        {
        }

        public virtual bool HasSurplus(Resource resource)
        {
            return false;
        }

        public virtual void Sell(Resource resource, decimal Qty, decimal Price)
        {
            this.Resources[resource] -= Qty;
            this.Resources[Resource.Money] += Price;
            TransactionEntries.Add(TransactionEntry.CreateEntry(MarketAction.Sell, resource, Qty, Price));
        }
    }
}
