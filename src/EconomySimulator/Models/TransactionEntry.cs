﻿using EconomySimulator.Enums;

namespace EconomySimulator.Models
{
    public class TransactionEntry
    {
        public MarketAction Action { get; set; }
        public Resource Item { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal TransactionCost
        {
            get { return Quantity * Price; }
        }

        public static TransactionEntry CreateEntry(MarketAction action, Resource item, decimal quantity, decimal price)
        {
            return new TransactionEntry()
            {
                Action = action,
                Item = item,
                Quantity = quantity,
                Price = price
            };
        }
    }
}
