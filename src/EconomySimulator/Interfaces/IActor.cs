﻿using EconomySimulator.Enums;

namespace EconomySimulator.Interfaces
{
    public interface IActor
    {
        string Identifier { get; }
        string Name { get; set; }
        Resource Outputs { get; }
        Dictionary<Resource, decimal> Resources { get; set; }

        void RunIteration();
        bool IsAlive();
        void Work();
        void Purchases(Resource resource, decimal Qty, decimal Price);
        void Sell(Resource resource, decimal Qty, decimal Price);

        /* Supply */
        bool HasSurplus(Resource resource);
        decimal GetSupplyForResource(Resource resource);

        /* Market Functions */
        decimal GetQtyToPurchase(Resource resource, decimal Price);
        bool CanPurchase(Resource resource, decimal Qty, decimal Price);

        /* Demand */
        List<Resource> GetResourceNeeds();
        bool HasDemandFor(Resource resource);
        decimal GetDemandForResource(Resource resource);
    }
}
