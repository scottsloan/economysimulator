﻿namespace EconomySimulator.Enums
{
    public enum Occupations
    {
        None,
        Farmer,
        WaterFetcher,
        Lumberjack,
    }
}
