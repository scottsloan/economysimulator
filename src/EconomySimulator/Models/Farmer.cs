﻿using EconomySimulator.Enums;

namespace EconomySimulator.Models
{
    public class Farmer : Person
    {
        public const int WATER_USAGE_WORK = 2;
        public const int FOOD_PRODUCE_WORK = 5;

        public Farmer()
            : base()
        {
            initialize();
        }

        public Farmer(string name)
            : base(name)
        {
            initialize();
        }

        private void initialize()
        {
            base.Outputs = Resource.Food;

            this.Purchases(Resource.Food, 10, 0);
            this.Purchases(Resource.Water, 10, 0);
            this.Purchases(Resource.Money, 50, 0);
        }

        public override void Work()
        {
            bool HasResourcesTodoWork = this.GetSupplyForResource(Resource.Water) >= WATER_USAGE_WORK;

            if (!HasResourcesTodoWork)
                return;

            this.Resources[Resource.Water] -= WATER_USAGE_WORK;
            this.Resources[Resource.Food] += FOOD_PRODUCE_WORK;
        }

        public override bool HasSurplus(Resource resource)
        {
            decimal amt = this.GetSupplyForResource(resource);

            if(resource == Resource.Water)
            {
                return amt > (WATER_USAGE_LIFE * 5) + WATER_USAGE_WORK;
            } else if (resource == Resource.Food)
            {
                return amt > FOOD_USAGE_LIFE * 2;
            }

            return true;
        }

        public override decimal GetDemandForResource(Resource resource)
        {
            decimal baseNeed = base.GetDemandForResource(resource);

            if (resource == Resource.Water)
            {
                baseNeed += WATER_USAGE_WORK;
            }

            return baseNeed;
        }
    }
}
