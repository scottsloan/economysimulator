﻿using EconomySimulator.Enums;

namespace EconomySimulator.Models
{
    public class WaterFetcher : Person
    {
        public const int WATER_PRODUCE_WORK = 5;

        public WaterFetcher()
            : base()
        {
            initialize();
        }

        public WaterFetcher(string name)
            : base(name)
        {
            initialize();
        }

        private void initialize()
        {
            base.Outputs = Resource.Water;
            this.Purchases(Resource.Food, 10, 0);
            this.Purchases(Resource.Water, 10, 0);
            this.Purchases(Resource.Money, 50, 0);
        }

        public override bool HasSurplus(Resource resource)
        {
            decimal amt = this.GetSupplyForResource(resource);

            if (resource == Resource.Water)
            {
                return amt > (WATER_USAGE_LIFE * 5);
            }
            else if (resource == Resource.Food)
            {
                return amt > FOOD_USAGE_LIFE * 2;
            }

            return true;
        }

        public override void Work()
        {
            bool HasResourcesTodoWork = true;

            if (!HasResourcesTodoWork)
                return;

            this.Resources[Resource.Water] += WATER_PRODUCE_WORK;
        }
    }
}
