﻿using EconomySimulator.Enums;
using EconomySimulator.Interfaces;

namespace EconomySimulator.Models
{
    public class Exchange
    {
        private Dictionary<Resource, int> Index;
        public Exchange()
        {
            Index = new Dictionary<Resource, int>
            {
                { Resource.Money, 1 },
                { Resource.Water, 1 },
                { Resource.Food, 5 },
                { Resource.Wood, 0 }
            };
        }

        public decimal GetMarketPriceForResource(Resource resource, List<IActor> population)
        {
            var _totalSupply = population.Select(x => x.GetSupplyForResource(resource)).Sum();
            var _totalDemand = population.Select(x => x.GetDemandForResource(resource)).Sum();

            decimal demandRato = 1 + (_totalDemand / _totalSupply);
            return Math.Round(Index[resource] * demandRato);
        }

        public void BuySellItem(IActor cosumer, List<IActor> population, Resource ItemToBuy)
        {
            var _producers = population.Where(p => p.Outputs == ItemToBuy
                                                           && p.HasSurplus(ItemToBuy))
                                                  .ToList();

            if (_producers.Count == 0)
                return;

            Random r = new Random();

            int selectedSeller = r.Next(_producers.Count());
            decimal price = this.GetMarketPriceForResource(ItemToBuy, population);
            decimal qty = cosumer.GetQtyToPurchase(ItemToBuy, price);
            decimal cost = qty * price;

            if (cosumer.CanPurchase(ItemToBuy, qty, cost))
            {
                IActor producer = _producers[selectedSeller];
                producer.Sell(ItemToBuy, qty, cost);
                cosumer.Purchases(ItemToBuy, qty, cost);
                Console.WriteLine($"\t {producer} sold  {qty} - {ItemToBuy} to {cosumer} for {cost} money");
            }
            else
            {
                Console.WriteLine($"\t {cosumer} can't afford {ItemToBuy}. Only {cosumer.GetSupplyForResource(Resource.Money)} money");
            }
        }
    }
}
