﻿using EconomySimulator.Enums;
using EconomySimulator.Interfaces;
using EconomySimulator.Models;

namespace EconomySimulator.CLI
{
    internal class Program
    {
        public static async Task Main(string[] args)
        {
            List<IActor> actors = new List<IActor>
            {
                new Farmer("Farmer Brown"),
                new WaterFetcher("Fetcher Pipes"),
                new Farmer("Farmer Calvin"),
                new WaterFetcher("Fetcher Nora")
            };

            int day = 0;
            Exchange market = new Exchange();

            while (actors.Count > 0)
            {
                day++;
                Console.WriteLine($"Day {day}");

                foreach (IActor actor in actors)
                {
                    actor.RunIteration();
                }

                foreach (IActor actor in actors)
                {
                    foreach(var r in actor.GetResourceNeeds())
                    {
                        market.BuySellItem(actor, actors, r);
                    }
                }

                PrintStats(actors);

                //Purge out the dead
                actors.RemoveAll(a => !a.IsAlive());
                await Task.Delay(10000);
            }
        }

        static void PrintStats(List<IActor> population)
        {
            Console.WriteLine("\t\t\t\t Food \t Water \t Money");
            foreach(var actor in population)
            {
                Console.WriteLine($"\t{actor.Name} \t\t {actor.GetSupplyForResource(Resource.Food)} \t {actor.GetSupplyForResource(Resource.Water)} \t {actor.GetSupplyForResource(Resource.Money)}");
            }
        }
    }
}