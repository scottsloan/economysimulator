﻿namespace EconomySimulator.Enums
{
    public enum MarketAction
    {
        Hold,
        Buy,
        Sell
    }
}
