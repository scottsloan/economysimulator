﻿namespace EconomySimulator.Enums
{
    public enum Resource
    {
        Money = 0,
        Food,
        Water,
        Wood
    }
}
