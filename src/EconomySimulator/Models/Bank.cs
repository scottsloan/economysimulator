﻿using EconomySimulator.Enums;
using EconomySimulator.Interfaces;

namespace EconomySimulator.Models
{
    public class Bank : IInstitution
    {
        public string Name { get; set; }
        public Dictionary<string, decimal> Accounts { get; set; }

        public Bank()
        {
            Accounts = new Dictionary<string, decimal>();
        }

        public void Deposit(IActor actor, Resource resource, decimal Qty)
        {
            //Create an account for this actor if there isn't one
            if (!Accounts.ContainsKey(actor.Identifier))
                Accounts.Add(actor.Identifier, 0);

            Accounts[actor.Identifier] += Qty;

            actor.Purchases(Resource.Money, -Qty, 0);
        }

        public void RunIteration()
        {

        }

        public decimal GetBalance(IActor actor)
        {
            if (!Accounts.ContainsKey(actor.Identifier))
                throw new Exception("Account not found");

            return Accounts[actor.Identifier];
        }

        public void Withdrawal(IActor actor, Resource resource, decimal Qty)
        {
            if (!Accounts.ContainsKey(actor.Identifier))
                throw new Exception("Account not found");

            decimal acctBalance = Accounts[actor.Identifier];

            if (Qty > acctBalance)
                throw new Exception("Funds not available");

            Accounts[actor.Identifier] -= Qty;

            actor.Purchases(Resource.Money, Qty, 0);
        }
    }
}
