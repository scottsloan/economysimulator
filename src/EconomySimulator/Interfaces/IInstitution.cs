﻿using EconomySimulator.Enums;

namespace EconomySimulator.Interfaces
{
    public interface IInstitution
    {
        string Name { get; set; }
        Dictionary<string, decimal> Accounts { get; set; }
        void RunIteration();
        void Deposit(IActor actor, Resource resource, decimal Qty);
        void Withdrawal(IActor actor, Resource resource, decimal Qty);
        decimal GetBalance(IActor actor);
    }
}
